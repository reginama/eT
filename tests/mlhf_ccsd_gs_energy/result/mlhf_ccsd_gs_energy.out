


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:30:28 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: 2 H2O close
        charge: 0
        multiplicity: 1
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm: mo-scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     solver cc gs
        omega threshold:  1.0d-11
        energy threshold: 1.0d-11
     end solver cc gs

     method
        mlhf
        ccsd
     end method

     active atoms
        selection type: list
        hf: {1, 2, 3}
     end active atoms


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: MLHF wavefunction
  =======================

  - MLHF settings:

     Occupied orbitals:    Cholesky
     Virtual orbitals:     PAOs

     Cholesky decomposition threshold:  0.10E-01

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O    -0.573030000000     2.189950000000    -0.052560000000        1
        2  H     0.347690000000     2.485980000000     0.050490000000        2
        3  H    -1.075800000000     3.019470000000     0.020240000000        3
        4  O    -1.567030000000    -0.324500000000     0.450780000000        4
        5  H    -1.211220000000     0.588750000000     0.375890000000        5
        6  H    -1.604140000000    -0.590960000000    -0.479690000000        6
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O    -1.082869761160     4.138405726491    -0.099324005107        1
        2  H     0.657038876250     4.697821351146     0.095412272029        2
        3  H    -2.032967364807     5.705971341340     0.038248056761        3
        4  O    -2.961257528977    -0.613216127421     0.851850742431        4
        5  H    -2.288874076596     1.112576255838     0.710329152963        5
        6  H    -3.031385265460    -1.116752550573    -0.906482724693        6
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               48
     Number of orthonormal atomic orbitals:   48

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         38
     Number of molecular orbitals:       48


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a MLHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (MO-SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (MO-SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - Setting initial AO density to sad

     Energy of initial guess:              -151.796506244373
     Number of electrons in guess:           20.000000000000

  - Active orbital space:

      Number of active occupied orbitals:        5
      Number of active virtual orbitals:        23
      Number of active orbitals:                28

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -151.986254697203     0.3356E+00     0.1520E+03
     2          -152.011877032057     0.1366E+00     0.2562E-01
     3          -152.018448490303     0.2167E-01     0.6571E-02
     4          -152.018690017977     0.5645E-02     0.2415E-03
     5          -152.018703912212     0.1006E-02     0.1389E-04
     6          -152.018704702590     0.1890E-03     0.7904E-06
     7          -152.018704720002     0.2445E-04     0.1741E-07
     8          -152.018704720373     0.5552E-05     0.3713E-09
     9          -152.018704720397     0.1150E-05     0.2396E-10
    10          -152.018704720398     0.2301E-06     0.8527E-12
    11          -152.018704720398     0.7198E-07     0.5684E-13
    12          -152.018704720397     0.1570E-07     0.1990E-12
    13          -152.018704720398     0.2151E-08     0.8527E-13
    14          -152.018704720398     0.5725E-09     0.2842E-13
    15          -152.018704720398     0.4648E-10     0.1137E-12
    16          -152.018704720398     0.8080E-11     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of MLHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.695204742952
     Nuclear repulsion energy:      37.386395233393
     Electronic energy:           -189.405099953791
     Total energy:                -152.018704720398

  - Summary of MLHF active/inactive contributions to electronic energy (a.u.):

     Active energy:               -104.805789875469
     Active-inactive energy:        19.262564524015
     Inactive energy:             -103.861874602338

  - Timings for the MLHF ground state calculation

     Total wall time (sec):              3.38300
     Total cpu time (sec):               6.30952


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    5
     Virtual orbitals:     23
     Molecular orbitals:   28
     Atomic orbitals:      48

   - Number of ground state amplitudes:

     Single excitation amplitudes:  115
     Double excitation amplitudes:  6670


  :: Ground state coupled cluster engine
  =========================================

  Calculates the ground state CC wavefunction | CC > = exp(T) | R >

  This is a CCSD ground state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    48
     Total number of shell pairs:           300
     Total number of AO pairs:             1176

     Significant shell pairs:               279
     Significant AO pairs:                 1073

     Construct shell pairs:                 300
     Construct AO pairs:                   1176

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1              1018 /     256       0.47383E+01         301             60             61080
     2               812 /     224       0.47071E-01         504            197            159964
     3               641 /     191       0.46926E-03         401            330            211530
     4               515 /     153       0.44731E-05         336            495            254925
     5               274 /      80       0.44603E-07         273            635            173990
     6               111 /      19       0.44172E-09         195            740             82140
     7                 0 /       0       0.43317E-11          40            762                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 762

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.9407E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1840E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              2.56900
     Total cpu time (sec):               3.90132


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_errors): file
     Storage (cc_gs_diis_parameters): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1          -152.224703416268     0.7638E-01     0.1522E+03
    2          -152.228974477794     0.2179E-01     0.4271E-02
    3          -152.232921954254     0.5776E-02     0.3947E-02
    4          -152.233529148104     0.1217E-02     0.6072E-03
    5          -152.233528796463     0.2569E-03     0.3516E-06
    6          -152.233541280768     0.7053E-04     0.1248E-04
    7          -152.233541468614     0.1823E-04     0.1878E-06
    8          -152.233541024860     0.5389E-05     0.4438E-06
    9          -152.233540954472     0.1675E-05     0.7039E-07
   10          -152.233540868876     0.5955E-06     0.8560E-07
   11          -152.233540895967     0.2020E-06     0.2709E-07
   12          -152.233540897466     0.7080E-07     0.1499E-08
   13          -152.233540900973     0.1974E-07     0.3507E-08
   14          -152.233540900548     0.4213E-08     0.4254E-09
   15          -152.233540900454     0.8468E-09     0.9402E-10
   16          -152.233540900453     0.2799E-09     0.1506E-11
   17          -152.233540900460     0.4641E-10     0.7276E-11
   18          -152.233540900458     0.1231E-10     0.1421E-11
   19          -152.233540900458     0.3090E-11     0.5969E-12
  ---------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):  -152.233540900458

     Correlation energy (a.u.):           -0.214836180060

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
       17      3        0.007388865462
        2      3       -0.006216991205
        1      4       -0.006192288073
        3      4       -0.005638161263
       18      4       -0.004977685922
        8      3        0.004508820648
        1      2        0.004134582614
        3      2       -0.003898985397
       14      4       -0.003294572626
        3      5        0.002836503232
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        7      5       7      5       -0.036354311873
        5      3       5      3       -0.034907556923
        2      3       2      3       -0.030557652144
        6      3       6      3       -0.027202606363
        5      4       5      4       -0.024500371178
        2      3       5      3       -0.021404928445
        8      3       8      3       -0.021113330150
        6      3       5      4        0.020537568180
        2      3       8      3       -0.019620728744
        7      5       9      5       -0.019219093908
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.64100
     Total cpu time (sec):               1.16591

  - Timings for the CCSD ground state calculation

     Total wall time (sec):              3.21100
     Total cpu time (sec):               5.06801

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 29.072180 MB

  Total wall time in eT (sec):              6.65300
  Total cpu time in eT (sec):              11.43978

  Calculation ended: 2021-10-06 10:30:34 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!
