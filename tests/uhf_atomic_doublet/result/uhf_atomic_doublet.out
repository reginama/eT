


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:32:26 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: F
        charge: 0
        multiplicity: 2
     end system

     method
        uhf
     end method

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     do
        ground state
     end do


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: UHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  F     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  F     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               14
     Number of orthonormal atomic orbitals:   14

  - Molecular orbital details:

     Number of alpha electrons:               5
     Number of beta electrons:                4
     Number of virtual alpha orbitals:        9
     Number of virtual beta orbitals:        10
     Number of molecular orbitals:           14


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a UHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-11
     Energy threshold:              0.1000E-11

  - Setting initial AO density to sad

     Energy of initial guess:               -99.127338116611
     Number of electrons in guess:            9.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-17
     Exchange screening threshold:   0.1000E-15
     ERI cutoff:                     0.1000E-17
     One-electron integral  cutoff:  0.1000E-22
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -99.369431839295     0.3171E-01     0.9937E+02
     2           -99.374748397856     0.7289E-02     0.5317E-02
     3           -99.375172207481     0.2867E-02     0.4238E-03
     4           -99.375232270176     0.7662E-03     0.6006E-04
     5           -99.375239767664     0.1642E-03     0.7497E-05
     6           -99.375240293420     0.2367E-04     0.5258E-06
     7           -99.375240303117     0.1456E-05     0.9697E-08
     8           -99.375240303129     0.3574E-07     0.1241E-10
     9           -99.375240303129     0.2199E-08     0.2842E-13
    10           -99.375240303129     0.3928E-09     0.2842E-13
    11           -99.375240303129     0.1792E-10     0.1421E-13
    12           -99.375240303129     0.2350E-12     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 12 iterations!

  - Summary of UHF wavefunction energetics (a.u.):

     HOMO-LUMO gap (alpha):          2.033127284876
     HOMO-LUMO gap (beta):           0.754022497815
     Nuclear repulsion energy:       0.000000000000
     Electronic energy:            -99.375240303129
     Total energy:                 -99.375240303129

  - UHF wavefunction spin expectation values:

     Sz:                   0.50000000
     Sz(Sz + 1):           0.75000000
     S^2:                  0.75200587
     Spin contamination:   0.00200587

  - Timings for the UHF ground state calculation

     Total wall time (sec):              0.68700
     Total cpu time (sec):               1.24129

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 70.428 KB

  Total wall time in eT (sec):              0.72500
  Total cpu time in eT (sec):               1.27918

  Calculation ended: 2021-10-06 10:32:26 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
