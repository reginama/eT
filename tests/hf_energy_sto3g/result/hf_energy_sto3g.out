


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:32:29 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
     end method


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: sto-3g
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  F    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: sto-3g
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  F    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               12
     Number of orthonormal atomic orbitals:   12

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:          2
     Number of molecular orbitals:       12


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

     Energy of initial guess:              -175.480886791665
     Number of electrons in guess:           20.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -175.004099522505     0.4891E-01     0.1750E+03
     2          -175.019694536059     0.1259E-01     0.1560E-01
     3          -175.020698892398     0.1221E-02     0.1004E-02
     4          -175.020716370362     0.2991E-03     0.1748E-04
     5          -175.020717278124     0.4457E-04     0.9078E-06
     6          -175.020717303672     0.6525E-05     0.2555E-07
     7          -175.020717304289     0.9652E-06     0.6171E-09
     8          -175.020717304297     0.7875E-07     0.8185E-11
     9          -175.020717304297     0.2017E-07     0.1137E-12
    10          -175.020717304297     0.7522E-08     0.2274E-12
    11          -175.020717304297     0.2208E-08     0.1705E-12
    12          -175.020717304297     0.3881E-09     0.1990E-12
    13          -175.020717304297     0.2928E-10     0.1421E-12
  ---------------------------------------------------------------
  Convergence criterion met in 13 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.801852651715
     Nuclear repulsion energy:      48.518317619727
     Electronic energy:           -223.539034924024
     Total energy:                -175.020717304297

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.12300
     Total cpu time (sec):               0.18239

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 32.304 KB

  Total wall time in eT (sec):              0.13900
  Total cpu time in eT (sec):               0.19854

  Calculation ended: 2021-10-06 10:32:30 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
