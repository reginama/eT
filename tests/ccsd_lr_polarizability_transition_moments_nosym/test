#!/usr/bin/env python3

from pathlib import Path
import sys

# we make sure we can import runtest and runtest_config
sys.path.append(str(Path(__file__).parents[1]))

# we import essential functions from the runtest library
from runtest import version_info, cli, run, get_filter

# this tells runtest how to run your code
from runtest_config import configure

# functions to get the filters
from filters import get_eom_filter, get_polarizability_filter

# we stop the script if the major version is not compatible
assert version_info.major == 2

# construct filter(s)
n_states = 4
threshold = 1.0e-9
f = get_eom_filter(n_states, threshold)

g = [
    get_filter(string="<< mu_x, mu_x >>(0.00E+00):", abs_tolerance=threshold),
    get_filter(string="<< mu_y, mu_y >>(0.00E+00):", abs_tolerance=threshold),
    get_filter(string="<< mu_y, mu_x >>(0.00E+00):", abs_tolerance=threshold),
    get_filter(string="<< mu_z, mu_z >>(0.00E+00):", abs_tolerance=threshold),
    get_filter(string="<< mu_z, mu_x >>(0.00E+00):", abs_tolerance=threshold),
    get_filter(string="<< mu_z, mu_y >>(0.00E+00):", abs_tolerance=threshold),
    get_filter(string="<< mu_x, mu_x >>(0.50E-01):", abs_tolerance=threshold),
    get_filter(string="<< mu_y, mu_y >>(0.50E-01):", abs_tolerance=threshold),
    get_filter(string="<< mu_y, mu_x >>(0.50E-01):", abs_tolerance=threshold),
    get_filter(string="<< mu_z, mu_z >>(0.50E-01):", abs_tolerance=threshold),
    get_filter(string="<< mu_z, mu_x >>(0.50E-01):", abs_tolerance=threshold),
    get_filter(string="<< mu_z, mu_y >>(0.50E-01):", abs_tolerance=threshold),
]

f.extend(g)


# invoke the command line interface parser which returns options
options = cli()

ierr = 0
for inp in ['ccsd_lr_polarizability_transition_moments.inp']:
    # the run function runs the code and filters the outputs
    ierr += run(options,
                configure,
                input_files=inp,
                filters={'out': f})

sys.exit(ierr)
