


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-11-30 15:22:51 UTC +01:00
  Git branch:         run-tests-with-increment
  Git hash:           1104323b813708ae3a904a1ced0d069b8c1cd95d
  Fortran compiler:   GNU 9.3.0
  C compiler:         GNU 9.3.0
  C++ compiler:       GNU 9.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                ON
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-11-30 15:28:57 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HFO He
        charge: 0
     end system

     do
        excited state
     end do

     memory
        available: 16
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:   1.0d-11
        energy threshold:  1.0d-11
     end solver cc gs

     solver cc multipliers
        threshold: 1.0d-11
     end solver cc multipliers

     solver cc es
        algorithm:              asymmetric lanczos
        chain length:           30
        lanczos normalization:  symmetric
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 16.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 6-31g
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000     0.100000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 6-31g
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457     0.188972612457    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               15
     Number of orthonormal atomic orbitals:   15

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:          9
     Number of molecular orbitals:       15


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -78.485481213937
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.759032131349     0.1061E+00     0.7876E+02
     2           -78.790350059646     0.8627E-01     0.3132E-01
     3           -78.805843546947     0.7149E-02     0.1549E-01
     4           -78.806172597235     0.2490E-02     0.3291E-03
     5           -78.806189589213     0.1804E-03     0.1699E-04
     6           -78.806189724492     0.1191E-04     0.1353E-06
     7           -78.806189726058     0.3312E-05     0.1566E-08
     8           -78.806189726243     0.8310E-06     0.1847E-09
     9           -78.806189726257     0.1348E-06     0.1423E-10
    10           -78.806189726257     0.5466E-08     0.7105E-13
    11           -78.806189726257     0.3040E-08     0.1421E-13
    12           -78.806189726257     0.9932E-09     0.2842E-13
    13           -78.806189726257     0.1716E-09     0.1421E-13
    14           -78.806189726257     0.1602E-10     0.0000E+00
    15           -78.806189726257     0.2373E-11     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.663415755506
     Nuclear repulsion energy:      12.163673938822
     Electronic energy:            -90.969863665079
     Total energy:                 -78.806189726257

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.12349
     Total cpu time (sec):               0.19543


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     9
     Molecular orbitals:   15
     Atomic orbitals:      15

   - Number of ground state amplitudes:

     Single excitation amplitudes:  54
     Double excitation amplitudes:  1485


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the multipliers (davidson algorithm)
     5) Calculation of the excited state (asymmetric lanczos algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    15
     Total number of shell pairs:            66
     Total number of AO pairs:              120

     Significant shell pairs:                65
     Significant AO pairs:                  119

     Construct shell pairs:                  66
     Construct AO pairs:                    120

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               103 /      56       0.47804E+01          54             20              2060
     2                68 /      44       0.36454E-01          68             51              3468
     3                39 /      27       0.27632E-03          46             79              3081
     4                12 /      10       0.25209E-05          27            103              1236
     5                 6 /       4       0.93493E-08           6            109               654
     6                 0 /       0       0.17298E-10           4            113                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 113

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.2185E-12
     Minimal element of difference between approximate and actual diagonal:  -0.3392E-15

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.02466
     Total cpu time (sec):               0.04866


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_errors): file
     Storage (cc_gs_diis_parameters): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -78.959425471287     0.7653E-01     0.7896E+02
    2           -78.963046178827     0.2739E-01     0.3621E-02
    3           -78.969448101702     0.7094E-02     0.6402E-02
    4           -78.970446876664     0.1934E-02     0.9988E-03
    5           -78.970519187991     0.4467E-03     0.7231E-04
    6           -78.970539390908     0.1909E-03     0.2020E-04
    7           -78.970524677962     0.3185E-04     0.1471E-04
    8           -78.970524620155     0.9060E-05     0.5781E-07
    9           -78.970524233015     0.3500E-05     0.3871E-06
   10           -78.970523969336     0.1357E-05     0.2637E-06
   11           -78.970523974729     0.4338E-06     0.5392E-08
   12           -78.970523975632     0.1846E-06     0.9032E-09
   13           -78.970523985409     0.7391E-07     0.9777E-08
   14           -78.970523980495     0.1792E-07     0.4915E-08
   15           -78.970523979661     0.3652E-08     0.8337E-09
   16           -78.970523979745     0.1130E-08     0.8438E-10
   17           -78.970523979781     0.3254E-09     0.3546E-10
   18           -78.970523979805     0.1219E-09     0.2453E-10
   19           -78.970523979815     0.3076E-10     0.1012E-10
   20           -78.970523979816     0.1098E-10     0.3411E-12
   21           -78.970523979816     0.3690E-11     0.8527E-13
  ---------------------------------------------------------------
  Convergence criterion met in 21 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -78.970523979816

     Correlation energy (a.u.):           -0.164334253558

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.018711443883
        7      4        0.013877408499
        9      5        0.008474530141
        2      4        0.007418075762
        5      6        0.007129422546
        6      2        0.006135657302
        8      5        0.003769047564
        6      5        0.003655323630
        3      4        0.002394375819
        9      2        0.001929291476
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.063683795110
        5      6       5      6       -0.047828674273
        8      3       8      3       -0.046755453136
        1      5       1      5       -0.043066597586
        2      4       1      5       -0.040185615943
        6      5       6      5       -0.039072469870
        1      4       1      4       -0.033308628671
        6      5       5      6        0.029051044046
        1      4       2      5       -0.028926359835
        2      5       2      5       -0.028720056230
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.01812
     Total cpu time (sec):               0.03412


  5) Calculation of the excited state (asymmetric lanczos algorithm)
     Calculating right vectors


  4) Calculation of the multipliers (davidson algorithm)

   - Davidson coupled cluster multipliers solver
  -------------------------------------------------

  A Davidson solver that solves the multiplier equation: t-bar^T A = -η. 
  This linear equation is solved in a reduced space. A description of 
  the algorithm can be found in E. R. Davidson, J. Comput. Phys. 17, 87 
  (1975).

   - Davidson tool settings:

     Number of parameters:                 1539
     Number of requested solutions:           1
     Max reduced space dimension:            50

     Storage (multipliers_davidson_trials): file
     Storage (multipliers_davidson_transforms): file

  - Davidson solver settings

     Residual threshold:              0.10E-10
     Max number of iterations:             100

  Iteration:                  1
  Reduced space dimension:    1

   Solution       Residual norm
  -----------------------------
     1               0.2588E-01
  -----------------------------

  Iteration:                  2
  Reduced space dimension:    2

   Solution       Residual norm
  -----------------------------
     1               0.6397E-02
  -----------------------------

  Iteration:                  3
  Reduced space dimension:    3

   Solution       Residual norm
  -----------------------------
     1               0.1201E-02
  -----------------------------

  Iteration:                  4
  Reduced space dimension:    4

   Solution       Residual norm
  -----------------------------
     1               0.5111E-03
  -----------------------------

  Iteration:                  5
  Reduced space dimension:    5

   Solution       Residual norm
  -----------------------------
     1               0.1391E-03
  -----------------------------

  Iteration:                  6
  Reduced space dimension:    6

   Solution       Residual norm
  -----------------------------
     1               0.3678E-04
  -----------------------------

  Iteration:                  7
  Reduced space dimension:    7

   Solution       Residual norm
  -----------------------------
     1               0.8315E-05
  -----------------------------

  Iteration:                  8
  Reduced space dimension:    8

   Solution       Residual norm
  -----------------------------
     1               0.2359E-05
  -----------------------------

  Iteration:                  9
  Reduced space dimension:    9

   Solution       Residual norm
  -----------------------------
     1               0.9066E-06
  -----------------------------

  Iteration:                 10
  Reduced space dimension:   10

   Solution       Residual norm
  -----------------------------
     1               0.4166E-06
  -----------------------------

  Iteration:                 11
  Reduced space dimension:   11

   Solution       Residual norm
  -----------------------------
     1               0.1344E-06
  -----------------------------

  Iteration:                 12
  Reduced space dimension:   12

   Solution       Residual norm
  -----------------------------
     1               0.3692E-07
  -----------------------------

  Iteration:                 13
  Reduced space dimension:   13

   Solution       Residual norm
  -----------------------------
     1               0.9265E-08
  -----------------------------

  Iteration:                 14
  Reduced space dimension:   14

   Solution       Residual norm
  -----------------------------
     1               0.2970E-08
  -----------------------------

  Iteration:                 15
  Reduced space dimension:   15

   Solution       Residual norm
  -----------------------------
     1               0.7382E-09
  -----------------------------

  Iteration:                 16
  Reduced space dimension:   16

   Solution       Residual norm
  -----------------------------
     1               0.2209E-09
  -----------------------------

  Iteration:                 17
  Reduced space dimension:   17

   Solution       Residual norm
  -----------------------------
     1               0.5922E-10
  -----------------------------

  Iteration:                 18
  Reduced space dimension:   18

   Solution       Residual norm
  -----------------------------
     1               0.1770E-10
  -----------------------------

  Iteration:                 19
  Reduced space dimension:   19

   Solution       Residual norm
  -----------------------------
     1               0.6756E-11
  -----------------------------
  Convergence criterion met in 19 iterations!

  - Davidson CC multipliers solver summary:

     Largest single amplitudes:
     -----------------------------------
        a       i         tbar(a,i)
     -----------------------------------
        1      5        0.028446007340
        7      4        0.024189655468
        9      5        0.015139982388
        5      6        0.010492409126
        6      2        0.010071850283
        2      4        0.009351248754
        6      5        0.007549205673
        8      5        0.006447407239
        3      4        0.006041547647
        4      5        0.004752031733
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         tbar(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.124605813771
        2      4       1      5       -0.099556655101
        6      5       5      6        0.098123955322
        5      6       5      6       -0.096442121927
        8      3       8      3       -0.093140163868
        2      4       5      6       -0.090022748788
        1      5       1      5       -0.083541815273
        7      4       5      6        0.080709431140
        6      5       6      5       -0.077411230259
        2      4       6      5        0.076047428593
     --------------------------------------------------

  - Finished solving the CCSD multipliers equations

     Total wall time (sec):               0.02224
     Total cpu time (sec):                0.04623

   - Asymmetric Lanczos excited state solver
  ---------------------------------------------

  An asymmetric Lanczos solver builds a reduced space tridiagonal representation 
  of the CC Jacobian of dimension defined by the chain length. Diagonalization 
  of this representation gives the eigenvalues and eigenvectors.

  A complete description of the algorithm can be found in  S.Coriani et 
  al., J. Chem. Theory Comput. 2012, 8, 5, 1616-1628.

  - Settings for coupled cluster excited state solver (asymmetric lanczos):

     Chain length:     30
     Biorthonormalization procedure: symmetric

  - Summary of the asymmetric Lanczos solver for excited states

     Printing the 10 lowest excited states for each Cartesian component 
     of the electric dipole moment

     Component: X

     State.      energy [a.u]         energy [eV]         Osc. strength
     ----------------------------------------------------------------------
        1        0.426107290346      11.594969963537      0.071314405977
        2        0.487873449882      13.275712773639      0.638605051358
        3        1.093451403277      29.754328228688      0.083177148051
        4        1.316249893542      35.816983951968      0.363258854228
        5        1.568056734064      42.668997091882      0.150449979175
        6        2.055432537450      55.931168214612      0.142089530282
        7        2.633078963878      71.649728107236      0.020492383458
        8        3.167548081851      86.193373592152      0.007787326566
        9        3.913106192916     106.481043152676      0.001571740382
       10        4.384261507551     119.301832294592      0.000187690418
     ----------------------------------------------------------------------
     For full spectrum see file: eT.lanczos30_X

     Component: Y

     State.      energy [a.u]         energy [eV]         Osc. strength
     ----------------------------------------------------------------------
        1        0.357563595121       9.729801013529      0.117412020225
        2        0.615429576304      16.746691768929      0.355762070121
        3        0.899148851531      24.467086488449      0.011122370175
        4        1.273250512328      34.646911191117      0.317748341808
        5        1.669928896455      45.441079827398      0.129711069510
        6        2.041902453872      55.562995887503      0.112396712052
        7        2.689534920447      73.185972934560      0.019374156295
        8        3.268884189147      88.950869525559      0.007494121423
        9        3.954783762508     107.615147587239      0.001518429794
       10        4.596574935279     125.079174933727      0.000202078347
     ----------------------------------------------------------------------
     For full spectrum see file: eT.lanczos30_Y

     Component: Z

     State.      energy [a.u]         energy [eV]         Osc. strength
     ----------------------------------------------------------------------
        1        0.249472929628       6.788504189641      0.004908591794
        2        0.825045644436      22.450635514874      0.007632160222
        3        1.005335921968      27.356583852436      0.105156155100
        4        1.527446664981      41.563940825761      0.260339663372
        5        1.733519932765      47.171480063841      0.358447812703
        6        2.106163510000      57.311628291841      0.110359966520
        7        2.773065817835      75.458964427981      0.021367360158
        8        3.414871508030      92.923386813704      0.005845343866
        9        4.112697017105     111.912186115734      0.001160146924
       10        4.786412586024     130.244920529289      0.000090901925
     ----------------------------------------------------------------------
     For full spectrum see file: eT.lanczos30_Z

  - Finished solving the CCSD excited state equations.

     Total wall time (sec):              0.24183
     Total cpu time (sec):               0.48131

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              0.30760
     Total cpu time (sec):               0.61507

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 1.370320 MB

  Total wall time in eT (sec):              0.44085
  Total cpu time in eT (sec):               0.82028

  Calculation ended: 2021-11-30 15:28:57 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!
